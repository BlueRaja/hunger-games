import random

from hunters import *
from constants import *

def runSimulation(tribe, numRoundsBeforeForceQuit, chanceOfForceQuit):
  currentRound = 0

  state = createAlgorithmInstances(tribe)
  deadHunters = []

  while not isGameOver(state, currentRound, numRoundsBeforeForceQuit, chanceOfForceQuit):
    # Start of round calculations
    currentRound += 1
    if currentRound % 100 == 0:
      print('Round %d' % currentRound)
    m = random.randrange(1, len(state)*(len(state)-1))

    # Set up the new state
    randomizeState(state)
    updateReputations(state)

    # Call hunt_choices() for each algorithm instance
    callHuntChoices(m, currentRound, state)
    updateHuntOutcomes(state)

    totalHunts = getTotalHunts(state)
    extraFood = getExtraFoodForRound(m, totalHunts, state)
    updateFood(extraFood, state)
    callRoundEnd(m, totalHunts, state)

    newlyDeadHunters = removeDeadHunters(state, currentRound)
    deadHunters.extend(newlyDeadHunters)

  printResults(state, currentRound, deadHunters)

def isGameOver(state, currentRound, numRoundsBeforeForceQuit, chanceOfForceQuit):
  if len(state) <= 1:
    return True
  return currentRound >= numRoundsBeforeForceQuit and random.random() <= chanceOfForceQuit

# empty class so I can add properties dynamically (lazy)
class emptyObject(object):
    pass

def createAlgorithmInstances(tribe):
  state = []

  for hunter in tribe:
      instance = emptyObject()
      instance.hunter = hunter
      instance.countHunts = 0
      instance.countSlacks = 0
      instance.countHuntBothCooperated = 0
      instance.countHuntBetrayedOtherPlayer = 0
      instance.countHuntWasBetrayed = 0
      instance.countHuntBothSlacked = 0
      state.append(instance)

  for instance in state:
    instance.food = 300 * (len(state)-1)

  return state

def randomizeState(state):
  random.shuffle(state)

def updateReputations(state):
  for instance in state:
    countTotal = instance.countHunts + instance.countSlacks + 0.0
    instance.reputation = instance.countHunts/countTotal if countTotal > 0 else 0.0

def callHuntChoices(m, numRounds, state):
  for index, instance in enumerate(state):
    otherPlayers = state[:index] + state[(index + 1):]
    playerReputations = [x.reputation for x in otherPlayers]
    instance.huntDecisions = instance.hunter.hunt_choices(numRounds, instance.food, instance.reputation, m, playerReputations)

def clearOldHuntOutcomes(state):
  for instance in state:
    instance.huntOutcomes = []
    instance.countHuntsThisRound = 0
    instance.countSlacksThisRound = 0

def updateHuntOutcomes(state):
  def huntOutcomeBothCooperated(instance):
    instance.huntOutcomes.append(0)
    instance.countHuntsThisRound += 1
    instance.countHuntBothCooperated += 1

  def huntOutcomeBetrayedOtherPlayer(instance):
    instance.huntOutcomes.append(1)
    instance.countSlacksThisRound += 1
    instance.countHuntBetrayedOtherPlayer += 1

  def huntOutcomeWasBetrayed(instance):
    instance.huntOutcomes.append(-3)
    instance.countHuntsThisRound += 1
    instance.countHuntWasBetrayed += 1

  def huntOutcomeBothSlacked(instance):
    instance.huntOutcomes.append(-2)
    instance.countSlacksThisRound += 1
    instance.countHuntBothSlacked += 1

  clearOldHuntOutcomes(state)
  for index1, instance1 in enumerate(state):
    for index2, instance2 in enumerate(state[(index1+1):]):
      huntIndexOf1 = index1
      huntIndexOf2 = index1 + index2
      hunter1Choice = instance1.huntDecisions[huntIndexOf2]
      hunter2Choice = instance2.huntDecisions[huntIndexOf1]

      if hunter1Choice == 'h' and hunter2Choice == 'h':
        huntOutcomeBothCooperated(instance1)
        huntOutcomeBothCooperated(instance2)
      elif hunter1Choice == 'h' and hunter2Choice == 's':
        huntOutcomeWasBetrayed(instance1)
        huntOutcomeBetrayedOtherPlayer(instance2)
      elif hunter1Choice == 's' and hunter2Choice == 'h':
        huntOutcomeBetrayedOtherPlayer(instance1)
        huntOutcomeWasBetrayed(instance2)
      else:
        huntOutcomeBothSlacked(instance1)
        huntOutcomeBothSlacked(instance2)

  for instance in state:
    instance.countHunts += instance.countHuntsThisRound
    instance.countSlacks += instance.countSlacksThisRound
    instance.hunter.hunt_outcomes(instance.huntOutcomes)

def getTotalHunts(state):
  return sum([instance.countHuntsThisRound for instance in state])

def getExtraFoodForRound(m, totalHunts, state):
  if totalHunts >= m:
    return 2 * (len(state) - 1)
  return 0

def updateFood(extraFood, state):
  for instance in state:
    foodDeltaFromHunts = sum(instance.huntOutcomes)
    instance.foodDeltaThisRound = foodDeltaFromHunts + extraFood
    instance.food += instance.foodDeltaThisRound

def callRoundEnd(m, totalHunts, state):
  for instance in state:
    instance.hunter.round_end(instance.foodDeltaThisRound, m, totalHunts)

def removeDeadHunters(state, currentRound):
  deadHunters = []
  for removeMe in [x for x in state if x.food <= 0]:
    removeMe.roundDied = currentRound
    deadHunters.append(removeMe)
    state.remove(removeMe)
  return deadHunters

def printResults(state, currentRound, deadHunters):
  print("-------------------------------------------------------")
  print("Game ended at round %d" % currentRound)
  print("-------------------------------------------------------")
  print("")
  print("The following hunters survived:")
  for instance in sorted(state, key=lambda x: -x.food):
    countTotal = instance.countHunts + instance.countSlacks
    print("  %s" % instance.hunter)
    print("    food       : %d" % instance.food)
    print("    reputation : %f" % instance.reputation)
    print("    Percentage of times...")
    print("        hunted  : %.2f" % (instance.countHunts*100.0/countTotal))
    print("        slacked : %.2f" % (instance.countSlacks*100.0/countTotal))
    print("        cooperated   (+0) : %.2f" % (instance.countHuntBothCooperated*100.0/countTotal))
    print("        betrayed     (+1) : %.2f" % (instance.countHuntBetrayedOtherPlayer*100.0/countTotal))
    print("        was betrayed (-3) : %.2f" % (instance.countHuntWasBetrayed*100.0/countTotal))
    print("        both slacked (-2) : %.2f" % (instance.countHuntBothSlacked*100.0/countTotal))
  print("")
  print("The following hunters died:")
  for instance in sorted(deadHunters, key=lambda x: -x.roundDied):
    countTotal = instance.countHunts + instance.countSlacks
    print("  %s" % instance.hunter)
    print("    round died : %d" % instance.roundDied)
    print("    food       : %d" % instance.food)
    print("    reputation : %f" % instance.reputation)
    print("    Percentage of times...")
    print("        hunted  : %.2f" % (instance.countHunts*100.0/countTotal))
    print("        slacked : %.2f" % (instance.countSlacks*100.0/countTotal))
    print("        cooperated   (+0) : %.2f" % (instance.countHuntBothCooperated*100.0/countTotal))
    print("        betrayed     (+1) : %.2f" % (instance.countHuntBetrayedOtherPlayer*100.0/countTotal))
    print("        was betrayed (-3) : %.2f" % (instance.countHuntWasBetrayed*100.0/countTotal))
    print("        both slacked (-2) : %.2f" % (instance.countHuntBothSlacked*100.0/countTotal))


if __name__ == "__main__":
  runSimulation(tribe, numRoundsBeforeForceQuit, chanceOfForceQuit)
