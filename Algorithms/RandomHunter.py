import random

# p is the probability of hunting
# p = 1 --> Cooperate bot
# p = 0 --> Defect bot

class Player():
  def __init__(self, p = 0.5):
    self.p = p
    pass

  def hunt_choices(self, round_number, current_food, current_reputation, m, player_reputations):
    hunt_decisions = ['h' if random.random() < self.p else 's' for x in player_reputations]
    return hunt_decisions

  def hunt_outcomes(self, food_earnings):
    pass # do nothing

  def round_end(self, award, m, number_hunters):
    pass # do nothing

  def __str__(self):
    return 'RandomHunter(%f)' % (self.p)
