import random
import math

# Cooperates (or betrays, if anti = True) with another hunter with probability 'rep'
# Cooperates less and less over time
class Player():
  def __init__(self, anti = False, scalingSlowness=500):
    self.anti = anti
    self.scalingSlowness = scalingSlowness
    pass

  def hunt_choices(self, round_number, current_food, current_reputation, m, player_reputations):
    if round_number == 1:
      probabilities = [0.5 for _ in player_reputations]
    elif self.anti:
      probabilities = [1 - x for x in player_reputations]
    else:
      probabilities = player_reputations

    hunt_decisions = ['h' if random.random() < prob/2**(round_number*1.0/self.scalingSlowness) else 's' for prob in probabilities]
    return hunt_decisions

  def hunt_outcomes(self, food_earnings):
    pass  # do nothing

  def round_end(self, award, m, number_hunters):
    pass  # do nothing

  def __str__(self):
    return 'RandomBasedOnRepSlacksOverTime(%s, %f)' % (self.anti, self.scalingSlowness)
