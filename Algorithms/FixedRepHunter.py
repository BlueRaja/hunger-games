import random

# Tries to maintain a fixed reputation, cooperating with other players of the highest reputation each round to maintain this
# If anti is set, cooperates with other players of lowest reputation

class Player():
  def __init__(self, rep = 0.9, anti = False):
    self.rep = rep
    self.sign = -1 if anti else 1

    self.huntCount = 0 # num times hunted 
    self.slackCount = 0 # num times slacked
    pass

  def hunt_choices(self, round_number, current_food, current_reputation, m, player_reputations):
    n = len(player_reputations)

    # choose h such that 
    # (self.huntCount + h) / (self.huntCount + self.slackCount + n) ~= self.rep
    # aka such that 
    h = int(round((self.huntCount + self.slackCount + n) * self.rep - self.huntCount))
    h = max(min(h, n), 0)

    player_rep_tuples = [(i, player_reputations[i]) for i in range(n)]
    player_rep_tuples.sort(key = lambda tup: - self.sign * tup[1])

    hunt_decisions = ['s' for i in range(n)]
    for i in range(min(h, n)):
      hunt_decisions[player_rep_tuples[i][0]] = 'h'

    self.huntCount += h
    self.slackCount += n-h
    return hunt_decisions

  def hunt_outcomes(self, food_earnings):
    pass # do nothing

  def round_end(self, award, m, number_hunters):
    pass # do nothing

  def __str__(self):
    return 'FixedRepHunter(%f, %d)' % (self.rep, self.sign)
