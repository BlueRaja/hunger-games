from Algorithms import *

###########################################
# SCENARIO 1
###########################################

tribe = [ \
  AdaptiveRepsHunter.Player() for i in range(1)
]  + [ \
AdaptiveRepHunter2.Player() for i in range(1)
]  + [ \
  AdaptivePercentile.Player() for i in range(1)
]  + [ \
  FixedRepHunter.Player(0.0) for i in range(5)
] + [ \
  FixedRepHunter.Player(0.5),
  FixedRepHunter.Player(0.55),
] + [ \
  FixedRepHunter.Player(1.0) for i in range(5)
] + [ \
  RandomHunter.Player(0.25),
  RandomHunter.Player(0.51),
  RandomHunter.Player(0.75),
] + [ \
  ThresholdHunter.Player(0.5, True) # troll
] + [ \
  ThresholdHunter.Player(0.4, False) for i in range(10)
] + [ \
  SlacksMoreOverTime.Player(100),
  SlacksMoreOverTime.Player(500),
  SlacksMoreOverTime.Player(2000),
] + [ \
  SmartHunter.Player(),
] + [ \
  BasedOnFood.Player(),
] + [ \
  RandomBasedOnRep.Player(True),
  RandomBasedOnRep.Player(False),
] + [ \
  RandomBasedOnRepSlacksOverTime.Player(True, 500),
  RandomBasedOnRepSlacksOverTime.Player(False, 500),
  RandomBasedOnRepSlacksOverTime.Player(True, 1000),
  RandomBasedOnRepSlacksOverTime.Player(False, 1000),
  RandomBasedOnRepSlacksOverTime.Player(True, 2000),
  RandomBasedOnRepSlacksOverTime.Player(False, 2000),
] + [ \
  BasedOnAverageRep.Player(),
  BasedOnAverageRep2.Player(),
] + [ \
  FixedPercentileHunter.Player(),
]

###########################################
# SCENARIO 2
###########################################
#
# tribe = [ \
# AdaptiveRepsHunter.Player() for i in range(1)
# ]  + [ \
# AdaptiveRepHunter2.Player() for i in range(1)
# ]  + [ \
# AdaptivePercentile.Player() for i in range(1)
# ]  + [ \
# FixedRepHunter.Player(0.0) for i in range(5)
# ] + [ \
# FixedRepHunter.Player(0.5),
# FixedRepHunter.Player(0.55),
# ] + [ \
# FixedRepHunter.Player(1.0) for i in range(5)
# ] + [ \
# RandomHunter.Player(0.25),
# RandomHunter.Player(0.51),
# RandomHunter.Player(0.75),
# ] + [ \
# ThresholdHunter.Player(0.5, True) # troll
# ] + [ \
# ThresholdHunter.Player(0.8, False) for i in range(20)
# ] + [ \
# SlacksMoreOverTime.Player(100),
# SlacksMoreOverTime.Player(500),
# SlacksMoreOverTime.Player(2000),
# ] + [ \
# SmartHunter.Player(),
# ] + [ \
# BasedOnFood.Player(),
# ] + [ \
# RandomBasedOnRep.Player(True),
# RandomBasedOnRep.Player(False),
# ] + [ \
# RandomBasedOnRepSlacksOverTime.Player(True, 500),
# RandomBasedOnRepSlacksOverTime.Player(False, 500),
# RandomBasedOnRepSlacksOverTime.Player(True, 1000),
# RandomBasedOnRepSlacksOverTime.Player(False, 1000),
# RandomBasedOnRepSlacksOverTime.Player(True, 2000),
# RandomBasedOnRepSlacksOverTime.Player(False, 2000),
# ] + [ \
# BasedOnAverageRep.Player(),
# BasedOnAverageRep2.Player(),
# ] + [ \
# FixedPercentileHunter.Player(),
# ]

