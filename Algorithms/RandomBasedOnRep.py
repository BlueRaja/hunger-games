import random
import math

# Cooperates (or betrays, if anti = True) with another hunter with probability 'rep'
class Player():
  def __init__(self, anti = False):
    self.anti = anti
    pass

  def hunt_choices(self, round_number, current_food, current_reputation, m, player_reputations):
    if round_number == 1:
      probabilities = [0.5 for _ in player_reputations]
    elif self.anti:
      probabilities = [1 - x for x in player_reputations]
    else:
      probabilities = player_reputations

    hunt_decisions = ['h' if random.random() < prob else 's' for prob in probabilities]
    return hunt_decisions

  def hunt_outcomes(self, food_earnings):
    pass  # do nothing

  def round_end(self, award, m, number_hunters):
    pass  # do nothing

  def __str__(self):
    return 'RandomBasedOnRep(%s)' % self.anti
