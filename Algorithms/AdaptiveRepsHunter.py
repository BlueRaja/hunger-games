import random

# Starts self at some reputation
# decreases own reputation by some increment until it stops being effective

# TODO: problem is that when trying to move reputation, it's usually either ALL defect or ALL cooperate
# causes simulations to be slightly degenerate

# scores based on own cooperation percentage and opponent's cooperation percentage
def score_coop_percentages(self_coop, opponent_coop):
  # hunting costs 4 extra energy, and brings only 3 food to self

  # score = 0
  # score += ((-4+3) * self_coop)
  # score += (3 * opponent_coop)

  return (3 * opponent_coop) - self_coop

class Player():
  def __init__(self, increment = 0.01):
    self.target_rep = 1
    self.increment = increment

    self.huntCount = 0 # num times hunted 
    self.slackCount = 0 # num times slacked

    self.lastRound_huntCount = 0 # num times hunted in the last round
    self.lastRound_slackCount = 0 # num times slacked in the last round
    self.lastRound_rep_index = 0
    self.lastRound = 0


    # maps a reputation percentage (rounded) to a score
    # keys are integers i, which represent the percentage i * increment
    self.rep_to_coop_percentage_map = {}
    pass

  def get_best_index(self):
    best_score = -float("Infinity")
    best_index = -1
    for index in self.rep_to_coop_percentage_map:
      score = self.rep_to_coop_percentage_map[index]
      if score > best_score:
        best_score = score
        best_index = index
    return index

  def hunt_choices(self, round_number, current_food, current_reputation, m, player_reputations):
    n = len(player_reputations)

    # choose h such that 
    # (self.huntCount + h) / (self.huntCount + self.slackCount + n) ~= target_rep
    # aka such that 
    h = int(round((self.huntCount + self.slackCount + n) * self.target_rep - self.huntCount))
    h = max(min(h, n), 0)

    player_rep_tuples = [(i, player_reputations[i]) for i in range(n)]
    player_rep_tuples.sort(key = lambda tup: - tup[1])

    hunt_decisions = ['s' for i in range(n)]
    for i in range(min(h, n)):
      hunt_decisions[player_rep_tuples[i][0]] = 'h'

    self.huntCount += h
    self.slackCount += n-h

    self.lastRound_huntCount = h
    self.lastRound_slackCount = n-h
    self.lastRound_rep = current_reputation
    self.lastRound = round_number

    return hunt_decisions

  def hunt_outcomes(self, food_earnings):
    # 3 * num_opponent_coop - (3 * self.lastRound_huntCount) - (2 * self.lastRound_slackCount) = sum(food_earnings) 
    num_opponent_coop = sum(food_earnings) + (3 * self.lastRound_huntCount) + (2 * self.lastRound_slackCount)
    assert num_opponent_coop % 3 == 0
    num_opponent_coop = num_opponent_coop / 3
    opponent_coop_percentage = (num_opponent_coop + 0.0) / (self.lastRound_huntCount + self.lastRound_slackCount)

    if self.lastRound == 1:
      return

    lastRound_rep_index = int(round(self.lastRound_rep / self.increment))

    score = score_coop_percentages(self.lastRound_rep, opponent_coop_percentage)
    self.rep_to_coop_percentage_map[lastRound_rep_index] = score
    #print
    #print 'last round target', self.target_rep
    #print 'last round index', lastRound_rep_index
    #print 'last round rep', self.lastRound_rep
    #print 'last round score', score
    #print 'last round opponent coop %', opponent_coop_percentage

    #print self.rep_to_coop_percentage_map
    #print self.target_rep

    # problem is it takes a while to actually reach best_index, even if it's the target
    # best_index = self.get_best_index(self)

    # this should happen rarely
    # if (best_index + 1) not in self.rep_to_coop_percentage_map:

    if (lastRound_rep_index + 1) not in self.rep_to_coop_percentage_map:
      # this is always true, unless the number of players is small enough relative to increment, and we overshoot our reputation
      if lastRound_rep_index == int(round(1 / self.increment)):
        self.target_rep = (lastRound_rep_index - 1) * self.increment
      else:
        # gap in existing reps!
        self.target_rep = (lastRound_rep_index + 1) * self.increment
      return
    else:
      up_score = self.rep_to_coop_percentage_map[lastRound_rep_index + 1]

      # go back up if it's good to
      if up_score > score:
        self.target_rep = (lastRound_rep_index + 1) * self.increment
        return

      if (lastRound_rep_index - 1) not in self.rep_to_coop_percentage_map:
        self.target_rep = (lastRound_rep_index - 1) * self.increment
        return
      low_score = self.rep_to_coop_percentage_map[lastRound_rep_index - 1]

      # go back down if it's good to
      if low_score > score:
        self.target_rep = (lastRound_rep_index - 1) * self.increment
      else:
        # stay where you are!
        self.target_rep = (lastRound_rep_index) * self.increment
      return

    # set self.target_rep

    pass # do nothing

  def round_end(self, award, m, number_hunters):
    pass # do nothing

  def __str__(self):
    return 'AdaptiveRepHunter'
