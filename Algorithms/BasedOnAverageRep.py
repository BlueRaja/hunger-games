import random
import math

# I noticed that, when most of the players are cooperative, the non-cooperative ones die off while everyone else survives.
# However, when most of the players are non-cooperative, the cooperative ones seem to die off first, followed by the cooperative ones.
# This class is designed to survive as long as possible in both scenarios
class Player():
  def __init__(self, anti = False):
    self.anti = anti
    pass

  def hunt_choices(self, round_number, current_food, current_reputation, m, player_reputations):
    if round_number == 1 or len([x for x in player_reputations if x >= 0.5]) >= len(player_reputations)/2:
      hunt_decisions = ['h' for _ in player_reputations]
    else:
      hunt_decisions = ['s' for _ in player_reputations]

    return hunt_decisions

  def hunt_outcomes(self, food_earnings):
    pass  # do nothing

  def round_end(self, award, m, number_hunters):
    pass  # do nothing

  def __str__(self):
    return 'BasedOnAverageRep()'
