import random
import math

###############
# PAYOFF MATRIX
###############

#   |    H    |    S    |
# H |  0 \  0 | -3 \  1 |
# S |  1 \ -3 | -2 \ -2 |

class Player():
  def __init__(self, repf = None):

    if repf is None:

      init_coop = 1 # cooperates this many rounds in a row
      falloff = 0.85 # lets the reputation eventually fall to this
      def repf(round_number, other_reps):
        return 0.80
        if round_number <= init_coop:
          return 1
        else:
          return falloff + (init_coop / round_number) * (1 - falloff) 

    self.repf = repf

    self.huntCount = 0 # num times hunted 
    self.slackCount = 0 # num times slacked
    pass

  def hunt_choices(self, round_number, current_food, current_reputation, m, player_reputations):
    n = len(player_reputations)

    # choose h such that 
    # (self.huntCount + h) / (self.huntCount + self.slackCount + n) ~= self.rep
    # aka such that 

    rep = self.repf(round_number, player_reputations);

    h = int(round((self.huntCount + self.slackCount + n) * rep - self.huntCount))
    h = max(min(h, n), 0)

    player_rep_tuples = [(i, player_reputations[i]) for i in range(n)]
    player_rep_tuples.sort(key = lambda tup: -tup[1])

    hunt_decisions = ['s' for i in range(n)]
    for i in range(min(h, n)):
      hunt_decisions[player_rep_tuples[i][0]] = 'h'

    self.huntCount += h
    self.slackCount += n-h
    return hunt_decisions

  def hunt_outcomes(self, food_earnings):
    pass # do nothing

  def round_end(self, award, m, number_hunters):
    pass # do nothing

  def __str__(self):
    return 'SmartHunter'
