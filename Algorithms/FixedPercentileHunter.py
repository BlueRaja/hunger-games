import random
import math

# Tries to stay within a certain percentile of reputation

class Player():
  def __init__(self, percentile = 50):
    self.percentile = percentile

    self.huntCount = 0 # num times hunted 
    self.slackCount = 0 # num times slacked
    pass

  def hunt_choices(self, round_number, current_food, current_reputation, m, player_reputations):
    rep = self.getPercentiledReputation(player_reputations)

    # Following code is copied from FixedRepHunter, I'm just going to assume it works

    n = len(player_reputations)

    # choose h such that 
    # (self.huntCount + h) / (self.huntCount + self.slackCount + n) ~= self.rep
    # aka such that 
    h = int(round((self.huntCount + self.slackCount + n) * rep - self.huntCount))
    h = max(min(h, n), 0)

    player_rep_tuples = [(i, player_reputations[i]) for i in range(n)]
    player_rep_tuples.sort(key = lambda tup: - tup[1])

    hunt_decisions = ['s' for _ in range(n)]
    for i in range(min(h, n)):
      hunt_decisions[player_rep_tuples[i][0]] = 'h'

    self.huntCount += h
    self.slackCount += n-h
    return hunt_decisions

  def getPercentiledReputation(self, player_reputations):
    index = int(self.percentile/100.0*(len(player_reputations) - 1))
    if len(player_reputations)%2 == 0 and index < len(player_reputations) - 1:
      return (player_reputations[index] + player_reputations[index+1])/2
    return player_reputations[index]

  def hunt_outcomes(self, food_earnings):
    pass # do nothing

  def round_end(self, award, m, number_hunters):
    pass # do nothing

  def __str__(self):
    return 'FixedPercentileHunter(%f)' % self.percentile
