import random

class Player():
  def __init__(self, scalingSlowness=500):
    """
    scalingSlowness defines how long it will take before the class really starts slacking off - lower numbers means it slacks off sooner
    """
    self.scalingSlowness=scalingSlowness
    pass

  def hunt_choices(self, round_number, current_food, current_reputation, m, player_reputations):
    #TODO: Can we make this smarter than just randomly deciding?  Does what FixedRepHunter do actually work better?
    hunt_decisions = ['h' if random.random() < 1/2**(round_number*1.0/self.scalingSlowness) else 's' for _ in player_reputations]
    return hunt_decisions

  def hunt_outcomes(self, food_earnings):
    pass # do nothing

  def round_end(self, award, m, number_hunters):
    pass # do nothing

  def __str__(self):
    return 'SlacksMoreOverTime(%f)' % self.scalingSlowness
