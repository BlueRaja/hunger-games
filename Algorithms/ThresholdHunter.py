import random
import math

# Cooperates or defects based on a threshold reputation
# If anti is set, then he defects against cooperative people and cooperates against defective people

class Player():
  # Threshold reputation for deciding whether to cooperate or defect
  def __init__(self, threshold = 0.5, anti = False):
    self.threshold = threshold
    self.sign = -1 if anti else 1
    pass

  def hunt_choices(self, round_number, current_food, current_reputation, m, player_reputations):
    if round_number == 1:
      hunt_decisions = ['h' for _ in player_reputations]
    else:
      hunt_decisions = ['h' if (rep * self.sign) > (self.threshold * self.sign) else 's' for rep in player_reputations]
    return hunt_decisions

  def hunt_outcomes(self, food_earnings):
    pass # do nothing

  def round_end(self, award, m, number_hunters):
    pass # do nothing

  def __str__(self):
    return 'ThresholdHunter(%f, %d)' % (self.threshold, self.sign)
