import random
import math

# Tries to stay within a certain rep
# Decreases or increases that rep over time, based on how much food it earns at that rep

class Player():
  def __init__(self):
    self.targetRep = 0.4
    self.foodPerHunterLastRound = 0

    self.huntCount = 0 # num times hunted 
    self.slackCount = 0 # num times slacked
    pass

  def hunt_choices(self, round_number, current_food, current_reputation, m, player_reputations):
    n = len(player_reputations)

    # choose h such that 
    # (self.huntCount + h) / (self.huntCount + self.slackCount + n) ~= self.rep
    # aka such that 
    h = int(round((self.huntCount + self.slackCount + n) * self.targetRep - self.huntCount))
    h = max(min(h, n), 0)

    player_rep_tuples = [(i, abs(self.targetRep-player_reputations[i])) for i in range(n)]
    player_rep_tuples.sort(key = lambda tup: - tup[1])

    hunt_decisions = ['s' for _ in range(n)]
    for i in range(min(h, n)):
      hunt_decisions[player_rep_tuples[i][0]] = 'h'

    self.huntCount += h
    self.slackCount += n-h
    return hunt_decisions

  def hunt_outcomes(self, food_earnings):
    foodPerHunterThisRound = sum(food_earnings)*1.0/len(food_earnings)
    if foodPerHunterThisRound < self.foodPerHunterLastRound:
      self.targetRep = min(self.targetRep+0.01, 1)
    else:
      self.targetRep = max(self.targetRep-0.01, 0)
    self.foodPerHunterLastRound = foodPerHunterThisRound

  def round_end(self, award, m, number_hunters):
    pass # do nothing

  def __str__(self):
    return 'AdaptiveRepHunter2()'
