import random

class Player():
  def __init__(self):
    pass

  def hunt_choices(self, round_number, current_food, current_reputation, m, player_reputations):
    if round_number == 1:
      self.startingFood = current_food
    percentageFoodLeft = (current_food*1.0 / self.startingFood)

    #TODO: Can we make this smarter than just randomly deciding?  Does what FixedRepHunter do actually work better?
    hunt_decisions = ['h' if random.random() < percentageFoodLeft else 's' for x in player_reputations]
    return hunt_decisions

  def hunt_outcomes(self, food_earnings):
    pass # do nothing

  def round_end(self, award, m, number_hunters):
    pass # do nothing

  def __str__(self):
    return 'BasedOnFood()'
